export default {
  user: null,
  likes: {
    likes: [],
    page: 1,
    totalLikes: 0,
    error: null,
    loading: false
  },
  dashboard: {
    posts: [],
    page: 1,
    error: '',
    loading: false
  },
  blog: {
    blog: {},
    error: null,
    loading: false,
    likes: []
  },
  bloggers: {
    bloggers: [],
    error: null,
    loading: false,
    sortProperty: 'name',
    sortDirection: 'asc'
  }
}