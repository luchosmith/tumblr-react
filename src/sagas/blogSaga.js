import { put, call, takeEvery } from 'redux-saga/effects'
import { FETCH_BLOG,
         FETCH_BLOG_ERROR,
         FETCH_BLOG_SUCCESS,
         FETCH_BLOG_LOADING
         } from '../actionTypes/'

import { blog } from '../api'
 
function* fetchBlog({blogName}) {
  yield put({ type: FETCH_BLOG_LOADING })
  try {
    const payload = yield call(blog, blogName)

    yield put({ type: FETCH_BLOG_SUCCESS, blog: payload })
  }
  catch(error) {
    yield put({ type: FETCH_BLOG_ERROR, error })
  }

}

export default function* saga() {
  yield takeEvery(FETCH_BLOG, fetchBlog)
}