import { all, call, fork } from 'redux-saga/effects'
import dashboardSaga from './dashboardSaga'
import bloggersSaga from './bloggersSaga'
import blogSaga from './blogSaga'
import bloggerLikesSaga from './bloggerLikesSaga'
import likesSaga from './likesSaga'
import userSaga from './userSaga'

export default function* rootSaga() {
  yield all([
    fork(dashboardSaga),
    fork(likesSaga),
    fork(bloggersSaga),
    fork(blogSaga),
    fork(userSaga)
  ])
}