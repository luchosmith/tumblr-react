import { eventChannel } from 'redux-saga'
import { put, call, take } from 'redux-saga/effects'
import { FETCH_USER_SUCCESS } from '../actionTypes'

function websocketInitChannel() {
  return eventChannel( emitter => {

    const socket = io.connect('http://localhost:3000')
    
    socket.on('userInfo', (user) => {
      console.log('emitting user', user)
      return emitter( { type: FETCH_USER_SUCCESS, user } )
    })

    // unsubscribe function
    return () => {
      // do whatever to interrupt the socket communication here
    }
  })
}

export default function* websocketSagas() {
  const channel = yield call(websocketInitChannel)
  while (true) {
    const action = yield take(channel)
    yield put(action)
  }
}