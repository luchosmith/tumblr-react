import { takeEvery } from 'redux-saga/effects'
import { FETCH_BLOGGER_LIKES,
         FETCH_BLOGGER_LIKES_LOADING,
         FETCH_BLOGGER_LIKES_ERROR,
         FETCH_BLOGGER_LIKES_SUCCESS } from '../actionTypes/'

import { bloggerLikes } from '../api'
 
function* fetchBloggerLikes({bloggerName}) {

  yield put({ type: FETCH_BLOGGER_LIKES_LOADING })

  try {
    const {bloggerLikes} = yield call(bloggeLikes, bloggerName)
    yield put({ type: FETCH_BLOGGER_LIKES_SUCCESS, bloggerLikes })
  }
  catch(error) {
    yield put({ type: FETCH_BLOGGER_LIKES_ERROR, error })
  }

}

export default function* saga() {
  yield takeEvery(FETCH_BLOGGER_LIKES, fetchBloggerLikes)
}