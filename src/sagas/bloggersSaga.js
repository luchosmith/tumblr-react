import { put, call, takeEvery } from 'redux-saga/effects'
import { FETCH_BLOGGERS, FETCH_BLOGGERS_ERROR, FETCH_BLOGGERS_SUCCESS, FETCH_BLOGGERS_LOADING } from '../actionTypes/'

import { bloggers } from '../api'
 
function* fetchBloggers() {
  yield put({ type: FETCH_BLOGGERS_LOADING })
  try {
    const payload = yield call(bloggers)
    yield put({ type: FETCH_BLOGGERS_SUCCESS, bloggers: payload })
  }
  catch(error) {
    yield put({ type: FETCH_BLOGGERS_ERROR, error })
  }

}

export default function* saga() {
  yield takeEvery(FETCH_BLOGGERS, fetchBloggers)
}