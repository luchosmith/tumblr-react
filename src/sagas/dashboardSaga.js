import { put, call, takeEvery } from 'redux-saga/effects'
import { FETCH_DASHBOARD,
         FETCH_DASHBOARD_LOADING,
         FETCH_DASHBOARD_ERROR,
         FETCH_DASHBOARD_SUCCESS } from '../actionTypes'
import { dashboard } from '../api'

function* fetchDashboard({since_id = 0, page = 1}) {
  yield put({ type: FETCH_DASHBOARD_LOADING })
  let params = `?since_id=${since_id}&page=${page}`

  try {
    const payload = yield call(dashboard, params)
    yield put({ type: FETCH_DASHBOARD_SUCCESS, ...payload })
  }
  catch(error) {
    yield put({ type: FETCH_DASHBOARD_ERROR, error })
  }

}

export default function* saga() {
  yield takeEvery(FETCH_DASHBOARD, fetchDashboard)
}