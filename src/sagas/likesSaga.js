import { put, call, takeEvery } from 'redux-saga/effects'
import { FETCH_LIKES,
         FETCH_LIKES_LOADING,
         FETCH_LIKES_SUCCESS,
         FETCH_LIKES_ERROR } from '../actionTypes'
import { getLikes } from '../api'

function* fetchLikes({before, after, page = 1}) {
  yield put({ type: FETCH_LIKES_LOADING })

  let params = ''

  if (before) {
    params = `?before=${before}`
  }
  if (after) {
    params = `?after=${after}`
  }

  try {
    const {likes, total} = yield call(getLikes, params, page)
    yield put({ type: FETCH_LIKES_SUCCESS, total, likes, page })
  }
  catch(error) {
    yield put({ type: FETCH_LIKES_ERROR, error })
  }

}

export default function* saga() {
  yield takeEvery(FETCH_LIKES, fetchLikes)
}