import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import configureStore from './store'
import rootSaga from './sagas'
import mainReducer from './reducers'
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom'

import Dashboard from './containers/dashboardContainer'
import Likes from './containers/likesContainer'
import Bloggers from './containers/bloggersContainer'
import Blog from './containers/blogContainer'
import Header from './components/header'

const store = configureStore()
// const theme = createMuiTheme();
const theme = createMuiTheme({
    palette: {
      primary: {
        // light: will be calculated from palette.primary.main,
        main: '#333333',
        // dark: will be calculated from palette.primary.main,
        // contrastText: will be calculated to contast with palette.primary.main
      },
      secondary: {
        light: '#0066ff',
        main: '#0044ff',
        // dark: will be calculated from palette.secondary.main,
        contrastText: '#ffcc00',
      },
      // error: will use the default color
    },
  });


class Root extends Component {

    constructor() {
        super();
        this.store = store
        this.state = this.store.getState()
    }

    render() {
        return (
            <Provider store={this.store}>
                <Router>
                    <MuiThemeProvider theme={theme}>
                        <Header />
                        <div className="main-content">
                            <Route path="/dashboard" component={Dashboard} />
                            <Route path="/likes" component={Likes} />
                            <Route path="/following" component={Bloggers} />
                            <Route path="/blog/:blogName" component={Blog} />
                        </div>
                    </MuiThemeProvider>
                </Router>
            </Provider>
        )
    }
}

ReactDOM.render((<Root />), document.getElementById('react-root'))
