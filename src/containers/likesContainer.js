import { connect } from 'react-redux'
import Likes from '../components/likes'
import { fetchLikes }from '../actions'


const mapStateToProps = state => {
  return {
    likes: state.likes.likes,
    page: state.likes.page,
    totalLikes: state.likes.totalLikes,
    error: state.likes.error,
    loading: state.likes.loading
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getLikes: (before, after, page) => {
      dispatch(fetchLikes(before, after, page))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Likes)