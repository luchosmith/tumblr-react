import { connect } from 'react-redux'
import Dashboard from '../components/dashboard'
import { fetchDashboard }from '../actions'

const mapStateToProps = state => {
  return {
    posts: state.dashboard.posts,
    page: state.dashboard.page,
    error: state.dashboard.error,
    loading: state.dashboard.loading
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getPosts: (since_id, page) => {
      dispatch(fetchDashboard(since_id, page))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard)