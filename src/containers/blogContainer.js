import { connect } from 'react-redux'
import Blog from '../components/blog'
import { fetchBlog }from '../actions'

const mapStateToProps = (state, ownProps) => {
  return {
    blog: state.blog.blog,
    likes: state.blog.likes,
    error: state.blog.error,
    loading: state.blog.loading,
    match: ownProps.match
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getBloginfo: (blogName) => {
      dispatch(fetchBlog(blogName))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Blog)