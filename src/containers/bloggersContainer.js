import { connect } from 'react-redux'
import Bloggers from '../components/bloggers'
import { fetchBloggers, sortBloggers }from '../actions'
import { getSortedBlogs } from '../selectors'

const mapStateToProps = state => {
  return {
    bloggers: getSortedBlogs(state.bloggers),
    error: state.bloggers.error,
    loading: state.bloggers.loading,
    sortProperty: state.bloggers.sortProperty,
    sortDirection: state.bloggers.sortDirection
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getBloggers: () => {
      dispatch(fetchBloggers())
    },
    sortBloggers: (property) => {
      dispatch(sortBloggers(property))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Bloggers)