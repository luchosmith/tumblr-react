
export const dashboard = (params) => {
  return fetch(`/tumblr/dashboard${params}`,  {
    credentials: 'same-origin'
  })
  .then(response => response.json())
  .then(likes => {
    return { posts:likes.posts }
  })
}

export const bloggers = () => {
    return fetch(`/tumblr/following`, {
      credentials: 'same-origin'
    })
    .then(response => response.json())
    .then(data => {
      return data.blogs
    })
  }
  
  export const blog = (blog) => {
    return fetch(`/tumblr/blogInfo?blog=${blog}`, {
      credentials: 'same-origin'
    })
    .then(response => response.json())
    .then(blogger => {
      return blogger.blog
    })
  }

  export const getLikes = (params, page) => {
    return fetch(`/tumblr/likes${params}`,  {
      credentials: 'same-origin'
    })
    .then(response => response.json())
    .then(likes => {
      return {
        likes: likes.liked_posts,
        total: likes.liked_count,
        page
      }
    })
  }

