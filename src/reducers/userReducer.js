import initialState from '../initialState'
import { FETCH_USER_SUCCESS } from '../actionTypes'

export default (state = initialState.user, action) => {

  switch(action.type) {

    case FETCH_USER_SUCCESS: {
      return {
        ...state,
        user: action.user
      }
    }

    default:
      return state;
  }
}