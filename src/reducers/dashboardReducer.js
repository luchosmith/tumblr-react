import initialState from '../initialState'
import { FETCH_DASHBOARD,
         FETCH_DASHBOARD_LOADING,
         FETCH_DASHBOARD_SUCCESS,
         FETCH_DASHBOARD_ERROR } from '../actionTypes'

const dashboardReducer = (state = initialState.dashboard, action) => {
  switch(action.type) {

    case FETCH_DASHBOARD_LOADING: {
      return {
        ...state,
        loading: true,
        error: false
      }
    }

    case FETCH_DASHBOARD_SUCCESS: {
      let posts = state.posts
      return {
        ...state,
        posts: action.posts,
        page: action.page,
        loading: false
      }
    }

    case FETCH_DASHBOARD_ERROR: {
      return {
        ...state,
        loading: false,
        error: true
      }
    }

    default: {
      return state;
    }
  }
}

export default dashboardReducer