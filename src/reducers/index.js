import { combineReducers } from 'redux'
import dashboardReducer from './dashboardReducer'
import bloggersReducer from './bloggersReducer'
import blogReducer from './blogReducer'
import likesReducer from './likesReducer'
import userReducer from './userReducer'

export default combineReducers({
    dashboard: dashboardReducer,
    bloggers: bloggersReducer,
    blog: blogReducer,
    likes: likesReducer,
    user: userReducer
});
