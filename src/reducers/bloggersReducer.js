import initialState from '../initialState'
import { FETCH_BLOGGERS_LOADING,
         FETCH_BLOGGERS_SUCCESS,
         FETCH_BLOGGERS_ERROR,
         SET_BLOGGER_SORT_OPTIONS } from '../actionTypes'

export default (state = initialState.bloggers, action) => {

  switch(action.type) {

    case FETCH_BLOGGERS_LOADING: {
      return {
        ...state,
        loading: true,
        error: false
      }
    }

    case FETCH_BLOGGERS_SUCCESS: {
      return {
        ...state,
        loading: false,
        bloggers: action.bloggers
      }
    }

    case FETCH_BLOGGERS_ERROR: {
      return {
        ...state,
        loading: false,
        error: true
      }
    }

    case SET_BLOGGER_SORT_OPTIONS: {
      let { property } = action
      if ( property === state.sortProperty ) {
        return {
          ...state,
          sortDirection: state.sortDirection === 'asc' ? 'desc' : 'asc'
        }
      } else {
        return {
          ...state,
          sortProperty: property,
          sortDirection: 'asc'
        }
      }
    }


    default:
      return state;
  }
}
