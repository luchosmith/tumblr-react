import initialState from '../initialState'
import { FETCH_LIKES_LOADING,
         FETCH_LIKES_SUCCESS,
         FETCH_LIKES_ERROR } from '../actionTypes'

export default (state = initialState.likes, action) => {

  switch(action.type) {

    case FETCH_LIKES_LOADING: {
      return {
        ...state,
        loading: true,
        error: false
      }
    }

    case FETCH_LIKES_SUCCESS: {
      return {
        ...state,
        likes: action.likes,
        totalLikes: action.total,
        page: action.page,
        loading: false
      }
    }

    case FETCH_LIKES_ERROR: {
      return {
        ...state,
        error: true,
        loading: false
      }
    }

    default:
      return state;
  }
}