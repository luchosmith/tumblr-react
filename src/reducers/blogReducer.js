import initialState from '../initialState'
import { FETCH_BLOG_LOADING,
         FETCH_BLOG_SUCCESS,
         FETCH_BLOG_ERROR,
         CLEAR_BLOG } from '../actionTypes'

export default (state = initialState.blogger, action) => {

  switch(action.type) {

    case FETCH_BLOG_LOADING: {
      return {
        ...state,
        loading: true,
        error: false
      }
    }

    case FETCH_BLOG_SUCCESS: {
      return {
        ...state,
        blog: action.blog,
        loading: false,
      }
    }

    case FETCH_BLOG_ERROR: {
      return {
        ...state,
        loading: false,
        error: true
      }
    }
    
    default:
      return {...state}
  }
}