import { FETCH_DASHBOARD,
         FETCH_LIKES,
         FETCH_BLOGGERS,
         SET_BLOGGER_SORT_OPTIONS,
         FETCH_BLOG,
         FETCH_BLOG_LIKES } from './actionTypes'

export const fetchDashboard = (since_id, page) => {
  return ({ type: FETCH_DASHBOARD, since_id, page })
}

export const fetchBloggers = () => {
  return({ type: FETCH_BLOGGERS })
}

export const sortBloggers = (property) => {
  return({ type: SET_BLOGGER_SORT_OPTIONS, property })
}

export const fetchBlog = (blogName) => {
  return({ type: FETCH_BLOG, blogName })
}

export const fetchLikes = (before, after, page) => {
  return({ type: FETCH_LIKES, before, after, page })
}


