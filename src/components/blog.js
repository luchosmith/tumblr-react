import React, { PureComponent } from "react"
import isEmpty from "lodash/isEmpty"
import Thumbnails from "./thumbnails"

  // Avatar sizes
  // 16, 24, 30, 40, 48, 64, 96, 128, 512

  // link to followed by
  // tumblr.com/followed/by/username

 
class Blog extends PureComponent {

    constructor(props) {
        super(props)
        this.blogName = props.match.params.blogName
    }

    componentWillMount() {
        if ( isEmpty(this.props.blog) || this.blogName !== this.props.blog.name ) {
            this.props.getBloginfo(this.blogName)
        }
    }

    renderLikes() {
        const blogger = this.props.blog
        return(
          <div>
            <h4>Likes</h4>
              <div className="tumblr-blogger-likes">
              <Thumbnails likes={blogger.liked_posts} />
            </div>
          </div>
        )
    }

    render() {
        const blogger = this.props.blog
        const loading = this.props.loading
        if(!loading) {
            return (
                <div className="container">
                    <div className="tumblr-blogger">
                        <div className="blogger-info-left">
                            <img src={`//api.tumblr.com/v2/blog/${blogger.name}/avatar/128`} />
                        </div>
                        <div className="blogger-info-right">
                            <div><a href={blogger.url} target="_blank">{blogger.name}</a></div>
                            <h3>{blogger.title} {blogger.is_nsfw ? <i className="fa fa-user-secret"></i>:""}
                                {blogger.followed ? <i className="fa fa-user-plus"></i>:""}
                                {blogger.share_likes ? <i class="fa fa-share-alt-square"></i>:""}</h3>
                            <div dangerouslySetInnerHTML={{__html: blogger.description}} />
                        </div>
                    </div>
                    <h4>Posts</h4>
                    <div className="tumblr-blogger-likes">
                        <Thumbnails likes={blogger.posts} />
                    </div>
                    {blogger.share_likes && this.renderLikes()}

                </div>
            )
        } else {
            return(<div>Loading...</div>)
        }
    }

}

export default Blog

// TODO: proptypes
/*
//   blogger: state.blogger.blogger,
//   likes: state.blogger.likes,
//   error: state.blogger.error,
//   loading: state.blogger.loading
//   getBlogger: () => {
//   getBloggerLikes: bloggerName => {
*/
