import React from "react"
import Button from '@material-ui/core/Button'
import { connect } from "react-redux"
import * as actions from '../actions'

const PageJumper = ({dispatch}) => {

    const handleSubmit = (event) => {
      event.preventDefault();
      dispatch(actions.fetchTumblrLikes({ likesPage: event.target.pagenum.value }));
    }
  
    return (
        <form onSubmit={handleSubmit}>
            <label>
            Jump to page:
            <input name="pagenum" type="text" />
            </label>
            <Button variant="raised" type="submit">Submit</Button>
        </form>
    )
    
  }

// const mapStateToProps = state => {
//     return {
//         bloggers : state.bloggers,
//         bloggerPage: state.bloggerPage
//     }
// }

export default connect()(PageJumper)