import React from 'react'
import Button from '@material-ui/core/Button'

const PaginatorButton = ({dir, onClick}) => {
    if (dir === 'prev') {
      return (
        <Button variant="raised" className="prev" onClick={onClick}>
          prev
        </Button>
      );
    } else {
      return (
        <Button variant="raised" className="next" onClick={onClick}>
          next
        </Button>
      );
    }
}

export default PaginatorButton;