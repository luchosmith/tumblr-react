import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

import PaginatorButton from './paginator-button';

class Bloggers extends PureComponent {

    componentWillMount() {
        if(!this.props.bloggers.length) {
            this.props.getBloggers()
        }
    }

    handleSort(property) {
        this.props.sortBloggers(property)
    }

    render() {
        const bloggers = this.props.bloggers
        const loading = this.props.loading
        const sortProperty = this.props.sortProperty
        const sortOrder = this.props.sortOrder
        if (loading) {
            // TODO: why u no working?
            return (<div>Loading...</div>)
        } else {
            return (
                <div>
                    <div className=" tumblr-blogger-outer-container">
                        {/* TODO: add filter by name/ description */}
                        <div>Sort:<span onClick={()=> this.handleSort('name')}> name </span>{' '}<span onClick={()=> this.handleSort('updatad')}> last updated </span></div>
                        <div className="tumblr-blogger-container">
                            { bloggers.map( blogger => {
                                return(
                                  <div className="blog" key={blogger.name}>
                                      <NavLink className="blog-name" to={`/blog/${blogger.name}`}>{blogger.name}</NavLink>
                                      <img src={`//api.tumblr.com/v2/blog/${blogger.name}/avatar/128`} />
                                      <a href={blogger.url} target="_blank" className="external-blog-link"><i className="fa fa-external-link"></i></a>
                                      <div className="last-updated">{new moment(blogger.updated).calendar()}</div>
                                  </div>
                                )
                              })
                            } 
                        </div>
                    </div>
                </div>
            )
        }
    }

}

export default Bloggers
