import React from 'react'
import PropTypes from 'prop-types'
import Thumbnail from './thumbnail'
import PaginatorButton from './paginator-button'

//TODO: x - y of z ?
//TODO: add onChange to validate input is number or use some other validation
//TODO: figure out why value isn't being updated. perhaps because it's stateless?
const Paginator = ({page=1, pageCount, prev=f=>f, next=f=>f, skip=f=>f}) =>
    <div className="row paginator">
        <PaginatorButton dir="prev" onClick={prev} />
            Page {page}
            <form onSubmit={event => skip(event)} style={{display:'inline-block',margin:'0px 10px'}}> 
              <input type="text" name="skipToPage" style={{color:'black',width:'45px'}} defaultValue={page} />
            </form>
        <PaginatorButton dir="next" onClick={next} />
    </div>

Paginator.proptypes = {
    page: PropTypes.number.isRequired,
    pageCount: PropTypes.number,
    prev: PropTypes.func.isRequired,
    next: PropTypes.func.isRequired
}

export default Paginator