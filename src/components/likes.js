import React, { PureComponent } from "react"
import PropTypes from "prop-types"
import Thumbnails from './thumbnails'
import PaginatorButton from './paginator-button'
import PageJumper from './page-jumper'
import * as actions from '../actions'

class Likes extends PureComponent {

    constructor(props) {
        super(props)
        this.prev = this.prev.bind(this)
        this.next = this.next.bind(this)
    }

    componentWillMount() {
        if(!this.props.likes.length) {
            this.props.getLikes()
        }
    }

    prev() {
        if (this.props.page > 1) {
            let after =  this.props.likes[0].liked_timestamp
            this.props.getLikes(null, after, this.props.page-1)
        }
    }
    // before, after, page
    // TODO: use destructuring
    next() {
        let before = this.props.likes[this.props.likes.length-1].liked_timestamp
        this.props.getLikes(before, null, this.props.page+1)
    }

    render(){
        return (
            <div>
                <div className="container tumblr-likes-container">
                    <div className="row">
                        <PaginatorButton dir="prev" onClick={this.prev} />
                            Page {this.props.page}
                        <PaginatorButton dir="next" onClick={this.next} />
                        {/* <PageJumper /> */}
                        Total Likes: {this.props.totalLikes}
                    </div>
                    <div className="row">
                        <Thumbnails likes={this.props.likes} />
                    </div>
                </div>
            </div>
        )
    }
}

Likes.propTypes = {
    likes: PropTypes.array.isRequired,
    page: PropTypes.number,
    error: PropTypes.bool,
    loading: PropTypes.bool,
    getLikes: PropTypes.func.isRequired
}

export default Likes
