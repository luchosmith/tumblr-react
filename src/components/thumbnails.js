import React from 'react'
import Thumbnail from './thumbnail'
import PostDetails from './post-details'

const Thumbnails = ({likes=[]}) => {
 return (<div className="thumbnail-wrapper">
      {likes.map(post => {
          return (
            <div key={post.id} className="post"> 
              <Thumbnail post={post} />
              <PostDetails post={post} />
            </div>
          )
      })}    
    </div>)
}

export default Thumbnails