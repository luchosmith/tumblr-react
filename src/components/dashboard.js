import React, { Component } from "react"
import PropTypes from "prop-types"
import Thumbnails from './thumbnails'
import PaginatorButton from './paginator-button'
import * as actions from '../actions'

class Dashboard extends Component {

    componentWillMount() {
      if(!this.props.posts.length) {
        this.props.getPosts()
      }
    }

    prev = () => {
      if (this.props.page > 1) {
        getPosts(this.props.posts[0].id, this.props.page-1)
      }
    }

    next = () => {
      getPosts(this.props.posts[posts.length-1].id, this.props.page +1 )
    }

    render() {
        return (
            <div>
                <div className="container tumblr-likes-container">
                    <div className="row">
                        <PaginatorButton dir="prev" onClick={this.prev} />
                            Page {this.props.page}
                        <PaginatorButton dir="next" onClick={this.next} />
                    </div>
                    <div className="row">
                    <div>
                        <Thumbnails likes={this.props.posts} />
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}

Dashboard.propTypes = {
    posts: PropTypes.array.isRequired,
    page: PropTypes.number,
    getPosts: PropTypes.func.isRequired
}

export default Dashboard

