import React from 'react'
//import PropTypes from 'prop-types'

const PostDetails = ({post}) => {

      return (
        <div className="post-details">
          <ul>
              <li><strong>ID:</strong>{post.id}</li>
              <li><strong>Blogger: </strong>{post.blog_name}</li>
              <li><strong>Followed: </strong>{post.followed}</li>
              <li><a href={post.post_url}>Original Post</a></li>
              <li><strong>Source: </strong>{post.source_title}</li>
              <li><strong>Number of photos: </strong>{post.photos ? post.photos.length : 0}</li>
          </ul>
        </div>
      );
}

//TODO: add proptypes

export default PostDetails;