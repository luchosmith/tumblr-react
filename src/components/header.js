import React, { Fragment } from 'react'
import { withStyles } from '@material-ui/core/styles'
import Appbar from '@material-ui/core/Appbar'
import Toolbar from '@material-ui/core/Toolbar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import { NavLink } from 'react-router-dom'

const styles = {
    root: {
      flexGrow: 1,
    },
    flex: {
      flex: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
  }

//TODO: figure out the whole Layout thing
const Header = (props) => {
    const { classes } = props
    return (
        <div className={classes.root}>
            <Appbar>
              <Toolbar>
                <Typography variant="title" color="inherit" className={classes.flex}>
                  Tumblr
                </Typography>
                <Tabs >
                  <Tab value={"dashboard"} label="Dashboard" component={NavLink} to="/dashboard" />
                  <Tab value={"likes"} label="Likes" component={NavLink} to="/likes"/>
                  <Tab value={"following"} label="Following" component={NavLink} to="/following" />
                </Tabs>
              </Toolbar>
            </Appbar>
        </div>
    )
}

export default withStyles(styles)(Header)
