import React from 'react'
import PropTypes from 'prop-types'


// TODO: implement hover events
const Thumbnail = ({post, onEnter=f=>f, onLeave=f=>f}) => {
  switch(post.type) {
    case 'photo':
      return (
        <div className={`grid-item ${post.type}`} >
          <a href={post.post_url} target="_blank">
          {/* <a href={post.photos[0].original_size.url} target="_blank"> */}
            <img src={post.photos[0].alt_sizes[post.photos[0].alt_sizes.length - 1].url} />
          </a>
        </div>
      )
    case 'video':
    return (
    <div className={`grid-item ${post.type}`} style={{backgroundImage: "url(" + post.thumbnail_url + ")"}} >
      <a href={post.post_url} target="_blank">
        <i className="fa fa-play-circle" aria-hidden="true"></i>
      </a>
  </div>
    );
    case 'text':
    return (
      <div className={`grid-item ${post.type}`}>
        <i className="fa fa-quote-left" aria-hidden="true"></i>
        <a href={post.post_url} target="_blank">Text</a>
      </div>
    )
    case 'link':
    return (
      <div className={`grid-item ${post.type}`}>
        <i className="fa fa-external-link" aria-hidden="true"></i>
        <a href={post.post_url} target="_blank">Link</a>
      </div>
    )
    default:
    return (
      <div className={`grid-item ${post.type}`}>
        <div>
          {post.type}:{post.id}
        </div>
      </div>
    )
}

}

Thumbnail.propTypes = {
  post: PropTypes.object.isRequired, //TODO: create a type for post
  onEnter: PropTypes.func,
  onLeave: PropTypes.func
}

export default Thumbnail;