import orderBy from 'lodash/orderBy'
import { createSelector } from 'reselect'

// selector
const getSortedBlogsSelector = (state) => {
return  orderBy(state.bloggers, state.sortProperty, state.sortDirectionz)
}

// reselect function
export const getSortedBlogs = createSelector(
  [ getSortedBlogsSelector ],
  (blogs) => blogs
)
