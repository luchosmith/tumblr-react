const express = require('express')
const router  = express.Router()
const _       = require('lodash')
const app     = require('./server')

router.get('/likes', function(req, res) {
  const limit = 40
  var startIndex = 0

  var page = parseInt(req.query.page)
  if (page > 1) {
    startIndex = (page-1) * limit;
  }
  var endIndex = startIndex + limit

  var result = _.uniqBy(app.liked_posts, function(e){
      return e.id
  })
  result = result.slice(startIndex, endIndex)
  res.json(result)
});

  // Post.find()
  //   .sort({liked_timestamp: sortDirection})
  //   .skip(offset)
  //   .limit(limit)
  //   .exec((err, results) => {
  //     if (err) {
  //       console.log(err)
  //     }
  //     res.json({
  //       liked_posts: results
  //     })
  //   })

    // router.get('/likes', (req, res) => {

  //   let page = req.params.page || 1
  //   page = parseInt(page)
  //   let limit = 40
  //   let offset = page === 1 ? 0 : (limit * page-1)
  //   let sortOrder = req.params.sortOrder || 'desc'

  //   function sortNewest(a,b) {
  //     return a.liked_timestamp > b.liked_timestamp
  //   }

  //   function sortOldest(a,b) {
  //     return a.liked_timestamp < b.liked_timestamp
  //   }

  //   User.find({name: req.session.userInfo.name}, 'liked_posts', (err, user) => {
  //     if (err) {
  //       console.log('error getting liked posts from DB', err)
  //       throw err
  //     }
  //     if (user && user.liked_posts) {
  //       if (sortOrder === 'desc') {
  //         user.liked_posts.sort(sortNewest)
  //       } else {
  //         user.liked_posts.sort(sortOldest)
  //       }
  //       let set = user.liked_posts.slice(offset,offset+limit)
  //       // query
  //       Post.find( { id : { $in : set } }, (err, posts) => {
  //         res.json({liked_posts: posts})
  //       } );
  //     } else {
  //       res.json({liked_posts:[]})
  //     }
  //   })
  
  // })

  

module.exports = router
