const express       = require('express')
const config        = require('../config/config')
const path          = require('path')
const logger        = require('morgan')
const bodyParser    = require('body-parser')
const session       = require('express-session')
const redisStore    = require("connect-redis")(session);
const redis         = require("redis").createClient()
const exphbs        = require('express-handlebars');
const tumblr        = require('tumblr.js')


module.exports = (app) => {
    app.use(logger('dev'))
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: false }))

    app.engine('handlebars', exphbs({defaultLayout: 'main'}));
    app.set('view engine', 'handlebars');

    app.use(session({
        secret: 'keyboard cat',
        cookie: {},
        resave: true,
        saveUninitialized: true,
        store: new redisStore({ host: config.redis.host, port: config.redis.port, client: redis})
      })
    )

    app.use((req, res, next) => {
      if (!req.session.oauth) {
          req.session.oauth = {};
          res.redirect('/auth/request')
      } else {
          next();
      }
    })

    app.use( (req, res, next) => {
        req.client = tumblr.createClient({
          credentials: {
          consumer_key    : process.env.tumblrkey,
          consumer_secret : process.env.tumblrsecret,
          token: req.session.oauth.tumblrOauthAccessToken,
          token_secret: req.session.oauth.tumblrOauthAccessTokenSecret
          },
          returnPromises: true
        })
        next()
      })

    // TODO: send back user data via socket
    app.use( (req, res, next) => {
      if (!req.session.userInfo) {
        req.client.userInfo()
          .then((data) => {
            app.socket.emit('userInfo', {user: data.user})
            req.session.userInfo = data.user
          })
          .catch((e) => {
            console.log(e)
            throw e
          })
      } else {
        app.socket.emit('userInfo', {user: req.session.userInfo})
      }
      next()
    })

    app.use(express.static(path.join(__dirname, '/../../public')))
    app.use(express.static(path.join(__dirname, '/../../node_modules')))
}


/*
        User.findOne({name: data.user.name}, (err, user) => {
          if (err) {
            console.log(err)
            throw err
          }
          if (!user) {
            User.create( {name: data.user.name, liked_count:0, liked_posts:[], blogs_followed:[]}, (err, user) => {
              if (err) {
                console.log('error saving user: ', err)
              } else {
                res.render('default', {
                  username: req.session.userInfo.name
                })
              }
            })
          }
          if (user) {
            res.render('default', {
              username: req.session.userInfo.name
            })
          }
        })
        */