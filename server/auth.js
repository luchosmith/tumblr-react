const express = require('express')
const oauth = require('oauth')
const router = express.Router()
const config = require('./config/config')


router.get('/request', function (req, res) {

  console.log('request', config.secrets)

  let port = config.port

  let url = `http://localhost:${port}`
  
  if (config.env === 'production') {
    url = "https://tumblr-react.herokuapp.com"
  }

  var consumer = new oauth.OAuth(
    "https://www.tumblr.com/oauth/request_token",
    "https://www.tumblr.com/oauth/access_token",
    config.secrets.tumblrkey,
    config.secrets.tumblrsecret,
    "1.0A",
    `${url}/auth/callback`,
    "HMAC-SHA1"
  )

  console.log('consumer', consumer)

  consumer.getOAuthRequestToken(function(error, oauthToken, oauthTokenSecret){
    if (error) {
      res.status(500).send(`Error getting OAuth request token: ${error.statusCode}: "${error.data}"`);
    } else {
    req.session.oauth = {};
    req.session.oauth.oauthRequestToken = oauthToken;
    req.session.oauth.oauthRequestTokenSecret = oauthTokenSecret;
    res.redirect("https://www.tumblr.com/oauth/authorize?oauth_token=" + oauthToken)
        }
    })
})


router.get('/callback', function (req, res, next) {
  let port = config.port

  let url = `http://localhost:${port}`
  
  if (config.env === 'production') {
    url = "https://tumblr-react.herokuapp.com"
  }

  var consumer = new oauth.OAuth(
    "https://www.tumblr.com/oauth/request_token",
    "https://www.tumblr.com/oauth/access_token",
    config.secrets.tumblrkey,
    config.secrets.tumblrsecret,
    "1.0A",
    `${url}/auth/callback`,
    "HMAC-SHA1"
  );
  var oauthRequestToken = req.session.oauth.oauthRequestToken;
  var oauthRequestTokenSecret = req.session.oauth.oauthRequestTokenSecret;
  consumer.getOAuthAccessToken(
	  oauthRequestToken,
	  oauthRequestTokenSecret,
	  req.query.oauth_verifier,
	  function(error, _oauthAccessToken, _oauthAccessTokenSecret) {
      if (error) {
        res.status(500).send(`Error getting OAuth access token: ${error.statusCode} ${error.data}`)
      } else {
        req.session.oauth.tumblrOauthAccessToken = _oauthAccessToken
        req.session.oauth.tumblrOauthAccessTokenSecret = _oauthAccessTokenSecret
        res.redirect('/')
      }
  })
})


module.exports = router
