var config = {
    dev: 'development',
    test: 'test',
    prod: 'production',
    port: process.env.PORT || 3000,
    secrets: {
        tumblrkey: process.env.tumblrkey,
        tumblrsecret: process.env.tumblrsecret
    },
    mongodb: {
        dsn: 'mongodb://localhost:27017/tumblr'
    },
    redis: {
        host: 'localhost',
        port: 6379
    }
}

process.env.NODE_ENV = process.env.NODE_ENV || config.dev
config.env = process.env.NODE_ENV

var envConfig

console.log(config.secrets)

try {
    envConfig = require('./' + config.env)
    envConfig = envConfig || {}
} catch(e) {
    console.log(e)
    envConfig = {}
}

module.exports = Object.assign(config, envConfig)
