const express = require('express')
const tumblr  = require('tumblr.js')
const moment = require('moment')

const router  = express.Router()
const request = require('request')

const Post = require('../../models/post')
const Blog = require('../../models/blog')
const User = require('../../models/user')

let app = require('../server')

const mapResponseObjects = (post) => {
  return {
    id,
    reblog_key,
    type,
    photos,
    body,
    post_url,
    slug,
    tags,
    blog_name,
    followed,
    note_count,
    thumbnail_url,
    source_url,
    source_title,
    date,
    timestamp,
    liked_timestamp
  } = post
}

function *getLikes(req) {
  
  let options = {
    limit: 20
  } 

  while(true) {
    try {
      let response = yield req.client.userLikes(options)
      console.log('response', response)
      if (response.liked_posts && response.liked_posts.length) {
        options.before = response.liked_posts[response.liked_posts.length-1].liked_timestamp
      } else {
        return //done
      }
    } catch(e) {
      console.log(e)
      throw e
    }
  }
}

//TODO: refactor this generator to be like the other one
function *getFollowing(req) {
  
  let options = {
    limit: 20,
    offset: 0
  } 

  while(true) {
    try {
      let response = yield req.client.userFollowing(options)
      let total_blogs = response.total_blogs
      response.blogs.forEach((blog) => {
        blog.updated = blog.updated * 1000 //actual milliseconds
        //TODO: don't put this here, use a req obj
        app.blogs[blog.name] = blog
      })
      options.offset += 20;
      if (response.blogs.length === 0) {
        return
      }
    } catch(e) {
      console.log(e)
      throw e
    }
  }
}

// TODO: throw 500 error on bad params
router.get('/following', (req, res) => {

  if (req.session.userFollowing) {
    let parsed = JSON.parse(req.session.userFollowing)
    var responseObj = {
      blogs: []
    }
    for (let p in parsed) {
      responseObj.blogs.push(parsed[p])
    }
    responseObj.blogs.sort((a,b)=>{
      return a.updated < b.updated
    })
    res.json(responseObj)
  } else {
    //TODO: send socket.io data for precentage complete
    // TODO: use req to pass objs around
    app.blogs = app.blogs || {}

    let iterator = getFollowing(req)
    let first = iterator.next()
    
    function iterate(iteration) {
      if (iteration.done) {
        req.session.userFollowing = JSON.stringify(app.blogs)
          //redis_client.set('lucho_following', JSON.stringify(app.blogs))
          var responseObj = {
            blogs: []
          }
          for (let b in app.blogs) {
            responseObj.blogs.push(app.blogs[b])
          }
          console.log(`Done fetching All Following`)
          res.json(responseObj)
          return
      }
      let promise = iteration.value
      promise
        .then((v)=>{
          iterate(iterator.next(v))
        })
        .catch((itErr)=>{
          console.log(itErr)
          throw itErr
        })
    }
  
    iterate(first)
  }

}) 


router.get('/likes', (req, res) => {

  let options = {
    limit: 30
  }

  if (req.query.after) {
    options.after = req.query.after
  }
  if (req.query.before) {
    options.before = req.query.before
  }

  req.client.userLikes(options)
    .then((data)=>{
      console.log('count', data.liked_posts.length)
      data.liked_posts = data.liked_posts.map(mapResponseObjects)
      res.json(data)
    })
    .catch((err)=>{
      console.log(err)
      throw err
    })

})

router.get('/syncLikes', (req, res) => {
console.log('syncLikes')
  let allLikedPosts = []
  let total_likes = 0;

  // TODO: factor out DB call
  User.findOne({name: req.session.userInfo.name}, 'liked_posts', (err, user) => {
    console.log('user.find', user)
    if (err) {
      console.log('error finding liked posts', err)
      throw err
    }
    if (user && user.liked_posts.length) {
      console.log('found a user and some likes')
      res.send(200, 'Already synched')
    }
    if (user && user.liked_posts.length === 0) {
      console.log('found user but no likes')
      let iterator = getLikes(req)
      let first = iterator.next()
      
      function iterate(iteration) {
        if (!iteration.done) {
          let promise = iteration.value
          promise
            .then((response) => {
              total_likes = response.liked_count
              Post.insertMany(response.liked_posts, {ordered:false})
              response.liked_posts.forEach((post) => {
                allLikedPosts.push({
                  id: post.id,
                  liked_timestamp: post.liked_timestamp
                })
              })
  
              iterate(iterator.next(response))
            })
            .catch((itErr)=>{
              console.log(itErr)
              throw itErr
            })
        } else {
          let query = { name: req.session.userInfo.name };
          User.findOneAndUpdate(query, { liked_posts: allLikedPosts, liked_count: total_likes }, (err, result) => {
            if (err) {
              console.log('some bullshit')
            } else {
              console.log('updated likes on user')
            }
          })
          console.log('Done synching all likes')
        }
      }
    
      iterate(first)
      res.send(200, 'OK')
    }
  })
  }) 


router.get('/bloglikes', (req, res) => {
  let blog = req.query.blog
  let offset = req.query.offset || 0
  let options = {
    limit: 20
  }
  req.client.blogLikes(blog, options)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      console.log(err)
      throw err
    })
})


router.get('/blogInfo', (req, res) => {
  let blog = req.query.blog
  req.client.blogInfo(blog)
  .then((data)=> {
    data.blog.posts = []
    data.blog.liked_posts = []
    let p = [];
    p.push( req.client.blogPosts(blog, {type:'photo'}) )
    if (data.blog.share_likes) {
      p.push( req.client.blogLikes(blog) )
    }
    Promise.all(p)
      .then((values) => {
        values.forEach((v,i) => {
          if (v.posts) {
            data.blog.posts = v.posts.map(mapResponseObjects)
          }
          if (v.liked_posts) {
            data.blog.liked_posts = v.liked_posts.map(mapResponseObjects)
          }
        })
        res.json(data)
      })
      .catch((e) => {throw e})
  })
  .catch((err)=>{
      throw Error(err)
  })
})

// TODO: is this even a thing?
// Can it be implemented with a scraper?
// router.get('blog/following)


router.get('/dashboard', function(req, res) {

  let options = {
    limit: 40,
    type: 'photo'
  }

  if (req.query.since_id) {
    options.since_id = req.query.since_id
  } else {
    options.since_id = 0
  }

  if (req.query.page && req.query.page >1 ) {
    options.offset = options.limit * parseInt(req.query.page)
  }

  req.client.userDashboard(options)
  .then((data)=> {
       data.posts = data.posts.map(mapResponseObjects)
       res.json(data)
  })
  .catch((err)=>{
    console.log(err)
    throw Error(err)
  })

})

// TODO: SOON...
// axios.get(`http://localhost:4000/tumblr/dashboard${params}`, {
  //   headers: {
  //     'X-My-Custom-Header': 'foobar',
  //     client_token: req.session.oauth.tumblrOauthAccessToken ,
  //     token_secret: req.session.oauth.tumblrOauthAccessTokenSecret
  //   }
  // })
  // .then((data)=> {
  //   data.posts = data.posts.map(mapResponseObjects)
  //   res.json(data)
  // })
  // .catch((err)=>{
  // console.log(err)
  // throw Error(err)
  // })


// TODO: verify this api call
router.get('/post', function(req, res) {
  let api_key = tumblrConsumerKey
  let blogger = req.query.b
  let id = req.query.id
  let url = 'https://api.tumblr.com/v2/blog/' + blogger + '/posts/?api_key=' + api_key + '&id=' + id
  request(url, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      let parsed = JSON.parse(response.body)
      let post = mapResponseObjects(parsed.response.posts[0])
      res.json(post) 
    }
  })
})

module.exports = router
