const express       = require('express')
const config        = require('./config/config')

const User = require('../models/user')

var app = module.exports = express()

require('./middleware')(app)

app.get('/', (req, res) => {
  res.redirect('/dashboard')
});

app.get('/dashboard', (req, res) => {
  res.render('default')
});

app.use('/auth', require('./auth'))
app.use('/tumblr', require('./tumblr/tumblr'))
// app.use('/api', require('./routes/api'));


//TODO: new phone, who dis?
app.get('/:filter?', function (req, res) {
  if (!req.params.filter) {
    res.redirect('/dashboard')
  }
  res.render('default')
});

app.get('/blog/:bloggerName', (req, res) => {
  res.render('default')
})


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error(`Not found:  ${req.path}`);
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
if (config.env === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    console.log('error', err);
    // res.render('error', {
    //   message: err.message,
    //   error: err
    // });
  });
}

// production error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.end(err.message);
});
