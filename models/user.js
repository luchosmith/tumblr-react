const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name: {type: String, unique:true},
    liked_posts: [],
    liked_count: Number,
    blogs_followed: []
  })

module.exports = mongoose.model('users', userSchema)
