const mongoose = require('mongoose')

const blogSchema = new mongoose.Schema({
    name: {type: String, unique:true},
    title: String,
    description: String,
    url: String,
    updated: Number
  })

mongoose.model('blogs', blogSchema)
