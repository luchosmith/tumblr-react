const mongoose = require('mongoose')

const postSchema = new mongoose.Schema({
    id: {type: String, unique:true},
    reblog_key: String, 
    type: String,
    photos: [],
    body: String,
    post_url: String,
    slug: String,
    tags: [],
    blog_name: String,
    followed: Boolean,
    note_count: Number,
    thumbnail_url: String,
    source_url: String,
    source_title: String,
    date: String,
    timestamp: Number,
    liked_timestamp: Number
})

module.exports = mongoose.model('posts', postSchema)